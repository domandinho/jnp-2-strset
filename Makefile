COMPILER=g++
COMMONCOMPILEFLAGS=-std=c++11 -c
ENABLED=1
DISABLED=0
DEBUGFLAGS=-D DEBUGLEVEL=$(ENABLED) -Wall 
RELEASEFLAGS=-D DEBUGLEVEL=$(DISABLED) -O2 
debuglevel ?=0
all:
ifeq ($(debuglevel), 0)
	$(COMPILER) $(COMMONCOMPILEFLAGS) $(RELEASEFLAGS) strsetconst.cc -o strsetconst.o
	$(COMPILER) $(COMMONCOMPILEFLAGS) $(RELEASEFLAGS) strset.cc -o strset.o
else
	$(COMPILER) $(COMMONCOMPILEFLAGS) $(DEBUGFLAGS) strsetconst.cc -o strsetconst.o
	$(COMPILER) $(COMMONCOMPILEFLAGS) $(DEBUGFLAGS) strset.cc -o strset.o
endif
