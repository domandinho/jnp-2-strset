#include <string>
#include <map>
#include <set>
#include <iostream>
#include <utility>
#include "strset.h"
#include "strsetconst.h"
using std::string;
using std::map;
using std::set;
using std::cerr;
#if DEBUGLEVEL == 0
static const bool debug = false;
#else
static const bool debug = true;
#endif

static map <unsigned long, set <string> > strsets;
static unsigned long new_id = 0;
const string snew = "strset_new: ";
const string sdelete = "strset_delete: ";
const string ssize = "strset_size: ";
const string sinsert = "strset_insert: ";
const string sremove = "strset_remove: ";
const string stest = "strset_test: ";
const string sclear = "strset_clear: ";
const string scomp = "strset_comp: ";
const string sset42 = "the Set 42\n";
static bool isInit = false;

void writeSetDontExist(const string& functionName, const unsigned long &id) {
	cerr << functionName << "set " << id << " does not exist\n";
}

void writeFunc2Arg(const string &funcName, const unsigned long &id, const string& s) {
	cerr << funcName << "(" << id << ", \"" << s << "\")\n";
}

bool isNull(const string& functionName, const char *value) {
	if (value == NULL) {
		if (debug) cerr << functionName << "element can't be null\n";
		return true;
	}
	return false;
}

bool isSet42(const unsigned long& id, const string &functionName,
	const string& communicate) {
	if (id == strset42) {
		if (debug) {
			cerr << functionName << communicate << sset42;
		}
		return true;
	}
	return false;
}

void initAll() {
	if (!isInit) {
		std::ios_base::Init();
		isInit = true;
		if (debug) {
			cerr << "strsetconst init invoked\n";
			cerr << "strset_new()\n";
			cerr << snew << "the Set 42 created\n";
			cerr << "strsetconst init finished\n";
		}
		set <string> empty;
		empty.insert("42");
		strsets.insert(std::make_pair(new_id, empty));
		new_id++;
	}
}

unsigned long strset_new() {
	initAll();
	if (debug) cerr << "strset_new()\n";
	if (debug) cerr << snew << "set " << new_id << " created\n";
	set<string> empty;
	strsets.insert(std::make_pair(new_id++, empty));
	return new_id - 1;
}

void strset_delete(unsigned long id) {
	initAll();
	if (debug) cerr << "strset_delete(" << id << ")\n";
	if (isSet42(id, sdelete, "you can't delete ")) return;
	unsigned short deleted = strsets.erase(id);
	if (debug) {
		if (deleted)
			cerr << sdelete << "set " << id << " deleted\n";
		else
			writeSetDontExist(sdelete, id);
	}
}

size_t strset_size(unsigned long id) {
	initAll();
	if (debug) cerr << "strset_size(" << id << ")\n";
	auto requested = strsets.find(id);
	if (requested != strsets.end()) {
		size_t t = requested->second.size();
		if (debug)
			cerr << ssize << "set " << id << " contains " << t
			<< " element(s)\n";
		return t;
	} else {
		if (debug)
			writeSetDontExist(ssize, id);
		return 0;
	}
}

void strset_insert(unsigned long id, const char* value) {
	initAll();
	if (isNull(sinsert, value)) return;
	string s(value);
	if (debug) writeFunc2Arg("strset_insert", id, s);
	if (isSet42(id, sinsert, "you can't insert elements to ")) return;
	auto requested = strsets.find(id);
	if (requested != strsets.end()) {
		auto p = requested->second.insert(s);
		if (debug) {
			if (p.second)
				cerr << sinsert << "set " << id << ", element \""
				<< s << "\" inserted\n";
			else {
				cerr << sinsert << "set " << id << " element \""
					<< s << "\" was already present\n";
			}
		}
	} else {
		if (debug)
			writeSetDontExist(sinsert, id);
	}
}

void strset_remove(unsigned long id, const char* value) {
	initAll();
	if (isNull(sremove, value)) return;
	string s(value);
	if (debug) writeFunc2Arg("strset_remove", id, s);
	if (isSet42(id, sremove, "you can't remove elements from ")) return;
	auto requested = strsets.find(id);
	if (requested != strsets.end()) {
		unsigned short removed = requested->second.erase(s);
		if (debug) {
			if (removed)
				cerr << sremove << "set " << id << ", element \""
				<< s << "\" removed\n";
			else
				cerr << sremove << "set " << id << " does not "
				"contain the element \"" << s << "\"\n";
		}
	} else {
		if (debug) {
			writeSetDontExist(sremove, id);
		}
	}
}

int strset_test(unsigned long id, const char* value) {
	initAll();
	if (isNull(stest, value)) return 0;
	if (debug) writeFunc2Arg("strset_test", id, value);
	auto requested = strsets.find(id);
	if (requested != strsets.end()) {
		string s(value);
		if (requested->second.find(s) != requested->second.end()) {
			if (debug)
				cerr << stest << "set " << id << " contains"
				<< " element \"" << s << "\"\n";
			return 1;
		} else {
			if (debug)
				cerr << stest << "set " << id << " does not "
				<< "contain element \"" << s << "\"\n";
			return 0;
		}
	} else {
		if (debug)
			writeSetDontExist(stest, id);
		return 0;
	}
}

void strset_clear(unsigned long id) {
	initAll();
	if (debug) cerr << "strset_clear(" << id << ")\n";
	if (isSet42(id, sclear, "you can't clear ")) return;
	auto requested = strsets.find(id);
	if (requested != strsets.end()) {
		requested->second.clear();
		if (debug)
			cerr << sclear << "set " << id << " cleared\n";
	} else {
		if (debug)
			writeSetDontExist(sclear, id);
	}
}

int compareSets(unsigned long id1, unsigned long id2) {
	auto itOne = strsets.find(id1);
	auto itTwo = strsets.find(id2);
	if (debug) {
		if (itOne == strsets.end()) writeSetDontExist(scomp, id1);
		if (itTwo == strsets.end()) writeSetDontExist(scomp, id2);
	}
	if (itOne == itTwo) return 0;
	if (itOne == strsets.end()) return -1;
	if (itTwo == strsets.end()) return 1;
	auto setItOne = itOne->second.begin();
	auto setItTwo = itTwo->second.begin();
	while (setItOne != itOne->second.end() &&
		setItTwo != itTwo->second.end()) {
		if (*setItOne < *setItTwo) return -1;
		if (*setItOne > *setItTwo) return 1;
		setItOne++;
		setItTwo++;
	}
	if (setItOne == itOne->second.end() &&
		setItTwo == itTwo->second.end()) return 0;
	if (setItOne == itOne->second.end()) return -1;
	else return 1;
}

int strset_comp(unsigned long id1, unsigned long id2) {
	initAll();
	if (debug) cerr << "strset_comp(" << id1 << ", " << id2 << ")\n";
	int result = compareSets(id1, id2);
	if (debug) {
		cerr << scomp << "result of comparing set " << id1 <<
			" to " << id2 << " is " << result << "\n";
	}
	return result;
}
