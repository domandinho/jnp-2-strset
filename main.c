#include <stdio.h>
#include <stdlib.h>
#include "strset.h"
#include "strsetconst.h"
#include <assert.h>

int main(int argc, char** argv) {
	assert(strset42 == 0);
	assert(strset_new() == 1);
	assert(strset_new() == 2);
	assert(strset_new() == 3);
	int id = strset42;
	strset_clear(id);
	strset_delete(id);
	strset_insert(id, "foo");
	strset_remove(id, "foo");
	assert(strset_size(id) == 1);
	strset_insert(1, NULL);
	strset_insert(2, NULL);
	strset_insert(3, NULL);
	strset_remove(1, NULL);
	assert(strset_test(2, NULL) == 0);
	strset_remove(3, NULL);
	assert(strset_size(1) == 0);
	assert(strset_size(2) == 0);
	assert(strset_size(3) == 0);
	strset_insert(1, "foo");
	strset_insert(1, "bar");
	strset_insert(1, "bar");
	assert(strset_size(1) == 2);
	strset_insert(1, "foo");
	assert(strset_size(1) == 2);
	
	return (EXIT_SUCCESS);
}

